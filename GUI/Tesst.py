# Import Libraries
import re
import nltk
import pandas as pd
import pygame
from pygame import mixer
import time
import os
import string
import random
from PIL import Image

image_dir = 'Image'
audio_dir = 'Audio'


# Game Init
pygame.init()
win = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
pygame.display.set_caption("smith chart GUI i hope")

mixer.init()

pygame.font.init()
font_1 = pygame.font.SysFont('impact', 50)
font_2 = pygame.font.SysFont('Arial', 25)
font_3 = pygame.font.SysFont('roboto', 35)
font_4 = pygame.font.SysFont('Arial', 40)
font_5 = pygame.font.SysFont('impact', 25)
font_6 = pygame.font.SysFont('impact', 120)
font_7 = pygame.font.SysFont('impact', 90)

#Colors
blue1   = (  3, 36, 77)
blue2   = ( 73,110,156)
orange1 = (221, 85, 12)
orange2 = (246,128, 38)
white   = (255,255,255)
black   = (  0,  0,  0)

#Click Counters- Sorry Will you know i <3 global variables
undo = 0
line = 0
circle = 0
clear = 0

clock = pygame.time.Clock()
pygame.time.set_timer(pygame.USEREVENT, 1000)

#############
# Main Page #
#############

page = 0

# Background
win.fill(white)

# Button LayOut

pygame.draw.rect(win, (blue2), (1370, 170, 175, 75))
undo_button = pygame.Rect(1370, 170, 175, 75)
win.blit(font_1.render('Undo', False, (white)), (1399, 175))

pygame.draw.rect(win, (blue2), (1370, 370, 175, 75))
line_button = pygame.Rect(1370, 370, 175, 75)
win.blit(font_1.render('Line', False, (white)), (1407, 375))

pygame.draw.rect(win, (blue2), (1370, 570, 175, 75))
circle_button = pygame.Rect(1370, 570, 175, 75)
win.blit(font_1.render('Circle', False, (white)), (1389, 575))

pygame.draw.rect(win, (blue2), (1370, 770, 175, 75))
clear_button = pygame.Rect(1370, 770, 175, 75)
win.blit(font_1.render('Clear', False, (white)), (1392, 775))

pygame.draw.rect(win, (blue2), (100, 770, 175, 75))
settings_button = pygame.Rect(100, 770, 175, 75)
win.blit(font_1.render('Settings', False, (white)), (101, 775))

# Actions
def everything_is_blue ():
    pygame.draw.rect(win, (blue2), (1370, 170, 175, 75))
    win.blit(font_1.render('Undo', False, (white)), (1399, 175))
    pygame.draw.rect(win, (blue2), (1370, 370, 175, 75))
    win.blit(font_1.render('Line', False, (white)), (1407, 375))
    pygame.draw.rect(win, (blue2), (1370, 570, 175, 75))
    win.blit(font_1.render('Circle', False, (white)), (1389, 575))
    pygame.draw.rect(win, (blue2), (1370, 770, 175, 75))
    win.blit(font_1.render('Clear', False, (white)), (1392, 775))
    pygame.draw.rect(win, (blue2), (100, 770, 175, 75))
    win.blit(font_1.render('Settings', False, (white)), (101, 775))

  ### Here is where we will put the lidar stuff that finds what pixel the puck is at
def get_point_a ():
    pygame.display.update()
    time.sleep(2)
    A = (255,255)
    return A

def get_point_b ():
    pygame.display.update()
    time.sleep(2)
    B = (555,555)
    return B

def get_radius ():
    pygame.display.update()
    time.sleep(2)
    r = 200
    return r

def undo_button_pressed():
    everything_is_blue()
    pygame.draw.rect(win, (orange2), (1370, 170, 175, 75))
    win.blit(font_1.render('Undo', False, (white)), (1399, 175))


def line_button_pressed():
    global line
    if (line == 1):
      everything_is_blue()
      pygame.draw.rect(win, (orange2), (1370, 370, 175, 75))
      win.blit(font_1.render('Line', False, (white)), (1407, 375))
      win.blit(font_5.render('Place puck on Point A', False, orange1), (40, 75))
      A = get_point_a()
      win.blit(font_5.render('Place puck on Point A', False, white), (40, 75))
      pygame.display.update
      win.blit(font_5.render('Place puck on Point B', False, orange1), (40, 75))
      B = get_point_b ()
      win.blit(font_5.render('Place puck on Point B', False, white), (40, 75))
      pygame.display.update()
      pygame.draw.line(win, black, A, B, 10)
      win.blit(font_5.render('If you are satisfied with the line, remove the puck', False, orange1), (40, 75))
    if (line == 2):
      win.blit(font_5.render('If you are satisfied with the line, remove the puck', False, white), (40, 75))
      everything_is_blue()
      line = 0


def circle_button_pressed():
    global circle
    if (circle == 1):
      everything_is_blue()
      pygame.draw.rect(win, (orange2), (1370, 570, 175, 75))
      win.blit(font_1.render('Circle', False, (white)), (1389, 575))
      win.blit(font_5.render('Place puck at center of circle', False, orange1), (40, 75))
      A = get_point_a()
      win.blit(font_5.render('Place puck at center of circle', False, white), (40, 75))
      pygame.display.update()
      win.blit(font_5.render('Place puck on circle', False, orange1), (40, 75))
      r = get_radius ()
      win.blit(font_5.render('Place puck on circle', False, white), (40, 75))
      pygame.display.update()
      pygame.draw.circle(win, black, A, r, 5)
      win.blit(font_5.render('If you are satisfied with the circle, remove the puck', False, orange1), (40, 75))
    if (circle == 2):
      win.blit(font_5.render('If you are satisfied with the circle, remove the puck', False, white), (40, 75))
      everything_is_blue()
      circle = 0

def clear_button_pressed():

    global clear
    if (clear == 1):
        everything_is_blue()
        pygame.draw.rect(win, (orange2), (1370, 770, 175, 75))
        win.blit(font_1.render('Clear', False, (white)), (1392, 775))
        win.blit(font_5.render('Are you sure you want to Clear?', False, orange1), (40, 75))
        win.blit(font_5.render('Remove puck to confirm', False, blue1), (40, 125))
    if (clear == 2):
        clear = 0
        win.fill(white)
        everything_is_blue()


def settings_button_pressed():
    everything_is_blue()
    pygame.draw.rect(win, (orange2), (100, 770, 175, 75))
    win.blit(font_1.render('Settings', False, (white)), (101, 775))
    pygame.quit()

#main loop- updates display when clicks occur
run = True
while run:
    pygame.time.delay(100)
    for event in pygame.event.get():

        if event.type == pygame.QUIT:
            run = False

        if event.type == pygame.MOUSEBUTTONDOWN:
            mouse_pos = event.pos
            if (undo_button.collidepoint(mouse_pos)) & (page == 0):
                undo_button_pressed()
                undo = undo+1
            if (line_button.collidepoint(mouse_pos)) & (page == 0):
                line = line + 1
                line_button_pressed()
            if (circle_button.collidepoint(mouse_pos)) & (page == 0):
                circle = circle +1
                circle_button_pressed()
            if (clear_button.collidepoint(mouse_pos)) & (page == 0):
                clear = clear+1
                clear_button_pressed()
            if (settings_button.collidepoint(mouse_pos)) & (page == 0):
                settings_button_pressed()


    pygame.display.update()

pygame.quit()