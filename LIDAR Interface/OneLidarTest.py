from serial import Serial
from time import sleep
from math import atan, pi
import matplotlib.pyplot as plt
import csv



port = "/dev/ttyUSB0"
br = 128000
readSize = 10000
ser = Serial(port, br)
ser.reset_input_buffer()

with open("testdata.csv", 'w', newline = "\n") as csvfile:
	csv = csv.writer(csvfile, dialect='excel')

	rad = []
	dist = []
	ang = []

	def calculate(hexdata):
		points = [] 
		if (hexdata[0]== 0): 	# not a zero package
			lsn = int.from_bytes(hexdata[1:2], byteorder = 'little')		#extracts the sample quantity
			fsa = int.from_bytes(hexdata[2:4], byteorder = 'little')		#extracts the starting angle
			lsa = int.from_bytes(hexdata[4:6], byteorder = 'little')		#extracts the ending angle
			cs  =  int.from_bytes(hexdata[6:8], byteorder = 'little')		#extracts the check sum
			hexdata = hexdata[8:]				#removes the starting bytes so only sampling data is left

			unCorrectedAngleStart = (float(fsa>>1))/64
			unCorrectedAngleEnd = (float (lsa>>1))/64

			if (len(hexdata)/2 == lsn):
				for i in range(0, len(hexdata) ,2):
					dist = int.from_bytes(hexdata[i:i+2], byteorder = 'little')
					if (dist == 0):
						angleCorrection = 0
					else:
						angleCorrection = atan(21.8*((155.3-dist)/(155.3*dist)))
					correctedAngleStart = unCorrectedAngleStart + angleCorrection
					correctedAngleEnd   = unCorrectedAngleEnd + angleCorrection
					angleDifference = correctedAngleEnd-correctedAngleStart

					if (angleDifference < 0):
						angleDifference = (360 - correctedAngleStart) + correctedAngleEnd

					if (i == 0):
						angle = correctedAngleStart
					elif (i/2 == lsn):
						angle = correctedAngleEnd
					else:
						angle = angleDifference/(lsn - 1)*(i) + correctedAngleStart
						if(angle > 360):
							angle -= 360

					points.append([dist,angle])

		return points

	def scan():
		points = []
		#ang = []

		ser.reset_input_buffer()
		ser.write(b"\xA5\x60")
		sleep (2)
		print (1)
		hexData = ser.read(readSize).split(b"\xAA\x55")[1:-1]
		#csv.writerows(hexData)
		#csv.writerow([" "," "])
		for i in hexData:
			calced = calculate(i)
		#	csv.writerows(calced)
			for a in calced:
				points.append(a)
		print(2)
		ser.write(b"\xA5\x65")
		print (3)
		sleep(1)
		ser.reset_input_buffer()

		for a in points:
			if (a != [] and a[0] != 0) :
				dist.append(a[0])
				ang.append(round(a[1],4))

		for deg in ang:
			rad.append(deg*pi/180)
		csv.writerow(dist)
		csv.writerow(ang)

	scan()
	ser.close()

	ax = plt.subplot(111, projection = 'polar')
	ax.scatter(rad, dist, s = 1)
	ax.set_title ("Lidar Plot")
	plt.savefig("Plot.png")







